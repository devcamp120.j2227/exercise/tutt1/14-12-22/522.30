import styled from "styled-components";

const Caculated = styled.button`
    font-size: 18px;
    width: 70px;
    height: 70px;
    border: 1px solid white;
    border-radius: 5px;
    ${props => props.type && props.type === "normal" ?
        `background-color: rgb(229,228,234); color: rgb(100,175,164);` :
        `background-color: rgb(185,215,213); color: rgb(112,112,114);`
    }
`

export default Caculated;