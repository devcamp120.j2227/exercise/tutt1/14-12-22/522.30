import styled from "styled-components";

const ButtonDel = styled.button`
    font-size: 18px;
    width: 140px;
    height: 70px;
    border-radius: 5px;
    border: 1px solid white;
    ${props => props.type && props.type === "normal" ?
        `background-color: rgb(206,210,216); color: rgb(112,112,114);` :
        `background-color: rgb(100,175,164); color: rgb(223,238,236);`
    }
`

export default ButtonDel;