import "./App.css";
import Caculated from "./components/styledButton";
import ButtonDel from "./components/styledButtonTop";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

function App() {
  return (
    <div className="div-main">
      <div className="div-card">
        <div className="div-header"></div>
        <div className="div-footer">
          <ButtonDel type="normal">DEL</ButtonDel>
          <Caculated style={{backgroundColor: "rgb(206,210,216)"}}><ArrowBackIcon/></Caculated>
          <Caculated>/</Caculated>
          <Caculated type="normal">7</Caculated>
          <Caculated type="normal">8</Caculated>
          <Caculated type="normal">9</Caculated>
          <Caculated>*</Caculated>
          <Caculated type="normal">4</Caculated>
          <Caculated type="normal">5</Caculated>
          <Caculated type="normal">6</Caculated>
          <Caculated>-</Caculated>
          <Caculated type="normal">1</Caculated>
          <Caculated type="normal">2</Caculated>
          <Caculated type="normal">3</Caculated>
          <Caculated>+</Caculated>
          <Caculated type="normal">0</Caculated>
          <Caculated type="normal">,</Caculated>
          <ButtonDel>=</ButtonDel>
        </div>
      </div>
    </div>
  );
}

export default App;
